var canvas;
var engine;
var camera;
var light;
var scene;
var loadedPlanet;
var hdrSkybox;

function initializeBabylonJS() {
    // Создание canvas
    var canvas = document.createElement("canvas");
        canvas.id = "gameCanvas";
        document.body.appendChild(canvas);
    // Создание движка
    engine = new BABYLON.Engine(canvas, true);
    // Создание сцены
    scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color4(0.3, 0.3, 0.3, 1.0);
    // Создание скайбокса
    var hdrTexture = BABYLON.CubeTexture.CreateFromPrefilteredData("https://assets.babylonjs.com/environments/environmentSpecular.env", scene);
    hdrTexture.rotationY = BABYLON.Tools.ToRadians(0);
    hdrSkybox = BABYLON.MeshBuilder.CreateBox("skybox", { size: 1024 }, scene);
    var hdrSkyboxMaterial = new BABYLON.PBRMaterial("skybox", scene);
    hdrSkyboxMaterial.backFaceCulling = false;
    hdrSkyboxMaterial.reflectionTexture = hdrTexture.clone();
    hdrSkyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
    hdrSkyboxMaterial.microSurface = 0.7;
    hdrSkyboxMaterial.disableLighting = true;
    hdrSkybox.material = hdrSkyboxMaterial;
    hdrSkybox.infiniteDistance = true;
    // Создание камеры
    camera = new BABYLON.FreeCamera('camera', new BABYLON.Vector3(0, 0, -8), scene);
    camera.setTarget(new BABYLON.Vector3(0, 0, 0));
    // Создание света
    light = new BABYLON.PointLight('light', new BABYLON.Vector3(1, 1, -3), scene);
    light.intensity = 10;
    // Загрузка модели
    BABYLON.SceneLoader.ImportMesh(null, 'js/Assets/', 'planet-low-poly.gltf', scene, function (meshArray) {
        // Создаем TransformNode для объединения всех мешей в группу
        loadedPlanet = new BABYLON.TransformNode("loadedPlanet", scene);
        // Переносим все загруженные меши в TransformNode
        meshArray.forEach(function (mesh) {
            if (mesh instanceof BABYLON.AbstractMesh) {
                mesh.parent = loadedPlanet;
            }
        });
        // Масштабируем группу
        loadedPlanet.scaling = new BABYLON.Vector3(1, 1, 1);
        // Дебаг режим для просмотра всякого внутри браузера.
        // scene.debugLayer.show();
    });
    // addListeners();
    engine.runRenderLoop(function () {
        scene.render();
        // swipeRotation();
    });

    window.addEventListener('resize', function() {
        engine.resize();
    });
};

window.function1 = function () {
    console.log("func 1");
    if (loadedPlanet) {
        loadedPlanet.rotation.y += -0.5; // Вращаем модель
    }
    else {
        console.log("Model is not loaded yet.");
    }
};

window.function2 = function () {
    console.log("func 2");
    if (loadedPlanet) {
        loadedPlanet.rotation.y += 0.5; // Вращаем модель
    }
    else {
        console.log("Model is not loaded yet.");
    }
};

window.function3 = function () {
    console.log("func 3");
    // Change material color randomly
    var colorsArray = [
        new BABYLON.Color3(1, 0, 0), // Красный
        new BABYLON.Color3(0, 1, 0), // Зеленый
        new BABYLON.Color3(0, 0, 1) // Синий
    ];
    var randomColor = colorsArray[Math.floor(Math.random() * colorsArray.length)];
    // boxMaterial.emissiveColor = randomColor;
};

window.function4 = function () {
    console.log("func 4");
    // Turn on/off automatically rotation for box:
    if (needAutoRotation) {
        stopAutoRotation();
    }
    else {
        startAutoRotation();
    }
    needAutoRotation = !needAutoRotation;
};

// Глобальная переменная для хранения интервала вращения
var rotationInterval = null;
var needAutoRotation = false;
// Функция для включения автоматического вращения по оси X
window.startAutoRotation = function () {
    if (!rotationInterval) {
        rotationInterval = setInterval(function () {
            if (loadedPlanet) {
                loadedPlanet.rotation.x += 0.025; // Увеличиваем угол вращения по оси X
                loadedPlanet.rotation.y += 0.015; // Увеличиваем угол вращения по оси X
                console.log(loadedPlanet.rotation);
            }
        }, 16); // Примерно 60 FPS
    }
};
// Функция для отключения автоматического вращения
window.stopAutoRotation = function () {
    if (rotationInterval) {
        clearInterval(rotationInterval);
        rotationInterval = null;
    }
};
// var isPointerDown = false;
// var startX, startY;
// var currentX, currentY;
// var velocityX = 0, velocityY = 0;
// var friction = 0.98;
// window.swipeRotation = function () {
//     if (!isPointerDown) {
//         velocityX *= friction;
//         velocityY *= friction;
//     }
//     ;
//     // console.log("loadedPlanet", loadedPlanet.rotation);
//     // loadedPlanet.rotation.y += velocityX;
//     // loadedPlanet.rotation.x += velocityY;
// };
// window.addListeners = function () {
//     canvas.addEventListener("pointerdown", function (event) {
//         isPointerDown = true;
//         startX = event.clientX;
//         startY = event.clientY;
//     });
//     canvas.addEventListener("pointermove", function (event) {
//         if (isPointerDown) {
//             currentX = event.clientX;
//             currentY = event.clientY;
//             // Вычисление скорости вращения
//             velocityX = (currentX - startX) * 0.01;
//             velocityY = (currentY - startY) * 0.01;
//             startX = currentX;
//             startY = currentY;
//         }
//     });
//     canvas.addEventListener("pointerup", function () {
//         isPointerDown = false;
//     });
// };
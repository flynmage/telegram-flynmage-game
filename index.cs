using System;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.ReplyMarkups;

class Program 
{
    private static readonly string Token = "6766287910:AAF41Ip6LtHtPO69jieyTp30M59XBVZRZJM";
    private static readonly string WebGLUrl = "https://game2.kodington.ru/telegram-flynmage-game/dist/index.html";
    private static readonly TelegramBotClient Bot = new TelegramBotClient(Token);

    static async Task Main(string[] args) {
        Bot.OnMessage += BotOnMessageReceived;
        Bot.StartReceiving();

        var commands = new[] 
        {
            new BotCommand { Command = "/start", Description = "Начальное приветствие"},
            new BotCommand { Command = "/info", Description = "Получить информацию о пользователе"},
            new BotCommand { Command = "/game", Description = "Запустить игру"},
        };

        await Bot.SetMyCommandsAsync(commands);

        Console.WriteLine("Bot started...");
        Console.ReadLine();
    }

    private static async void BotOnMessageReceived(object sender, MessageEventArgs e) 
    {
        var message = e.Message;
        if (message == null || message.Type != Telegram.Bot.Types.Enums.MessageType.Text) return;

        var chatId = message.Chat.Id;
        var text = message.Text;

        if (text == "/start") 
        {
            await Bot.SendTextMessageAsync(chatId, "Добро пожаловать!");
        } 
        else if (text == "/info") 
        {
            await Bot.SendTextMessageAsync(chatId, $"Тебя зовут {message.Chat.FirstName} {message.Chat.LastName}");
        } 
        else if (text == "/game") 
        {
            var messageText = "Войди в игру по кнопке ниже!";
            var keyboard = new ReplyKeyboardMarkup(new[]
            {
                new []
                {
                    new KeyboardButton("Open App")
                    {
                        WebApp = new WebAppInfo(WebglUrl)
                    }
                }
            })
            {
                ResizeKeyboard = true,
                OneTimeKeyboard = true
            };

            await Bot.SendTextMessageAsync(chatId, messageText, replyMarkup: keyboard);
        }
        else
        {
            await Bot.SendTextMessageAsync(chatId, "Я тебя не понимаю, попробуй повторить запрос еще раз");
        }
    }
}
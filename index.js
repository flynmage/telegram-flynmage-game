const TelegramApi = require('node-telegram-bot-api')

const token = '6766287910:AAF41Ip6LtHtPO69jieyTp30M59XBVZRZJM'
const WEBGL_URL = 'https://www.flynmage-telegram.ru';

const bot = new TelegramApi(token, { polling: true })

const start = () => {
    bot.setMyCommands([
        { command: '/start', description: 'Начальное приветствие' },
        { command: '/info', description: 'Получить информацию о пользователе' },
        { command: '/game', description: 'Запустить игру' },
    ])

    bot.on('message', async msg => {
        const text = msg.text;
        const chatId = msg.chat.id;

        if (text === '/start') {
            return bot.sendMessage(chatId, 'Добро пожаловать')
        }

        if (text === '/info') {
            return bot.sendMessage(chatId, `Тебя зовут ${msg.chat.first_name} ${msg.chat.last_name}`)
        }

        if (text === '/game') {
            const messageText = `Войти в игру BabylonJS по кнопке ниже!`
            const opts = {
                reply_to_message_id: msg.message_id,
                reply_markup: {
                    resize_keyboard: true,
                    one_time_keyboard: true,
                    keyboard: [
                      [{
                        text: 'Open App', 
                        web_app: { 
                            url: WEBGL_URL 
                        }
                      }]
                    ]
                }
            }

            return bot.sendMessage(chatId, messageText, opts)
        }
        // console.log(msg)
        return bot.sendMessage(chatId, 'Я тебя не понимаю, попробуй повторить запрос еще раз')
    })
}

start()